package pilaspro;
//Un package es una agrupación de clases afines 
//Una clase puede definirse como perteneciente a un package 
//y puede usar otras clases definidas en ese o en otros packages.
import java.io.*;
//Para importar clases de un paquete se usa el comando import.
//Se puede importar una clase individual
//o bien, se puede importar las clases declaradas públicas de un paquete completo
import javax.swing.JOptionPane;
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//package pilaspro;

public class Pilaspro {
    //Las clases en Java son básicamente una plantilla que sirve para crear un objeto,
//ya sea de forma publica o privada.
   public static final int MAX_LENGTH = 5;
   //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   public static String Pilapro[] = new String[MAX_LENGTH];
   //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   public static int cima = -1;
     //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //int: declara valores enteres los cuales no tendran retorno dentro del metodo implementado
   public static String Pilaaux[] = new String[MAX_LENGTH];
     //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //pilaaux:almacenamiento de datos ingresados y enviados a la variable declarada como pilaaux
   //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase.
   public static int cimaaux = -1;
   //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //int: declara valores enteres los cuales no tendran retorno dentro del metodo implementado
   public static String Pilaaux2[] = new String[MAX_LENGTH];
     //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase.
   public static int cimaaux2 = -1;
     //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //int: declara valores enteres los cuales no tendran retorno dentro del metodo implementado
   public static String Pila1[] = new String[MAX_LENGTH];
   //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase.
   public static int cima1 = -1;
      //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //int: declara valores enteres los cuales no tendran retorno dentro del metodo implementado
   public static String Pila2[] = new String[MAX_LENGTH];
    //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase.
   public static int cima2 = -1;
     //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
   //int: declara valores enteres los cuales no tendran retorno dentro del metodo implementado
        public static void main(String[] args)throws IOException {
            //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
   //main(String[] args): Nombre del método principal para ejecutar
   // TODO code application logic here
   //throws: funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
            Menu();
            //conformacion del menu de las pilas
        }
    
    public static void Menu()throws IOException{
        //throws: funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
       String salida="====Menú de diferencia entre Pilas====\n"+"1- Ingresar a Pila 1\n"+"2- Ingresar a Pila 2\n";
        //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase.
       salida=salida+"3- Comparar pila 1 y 2\n"+"4- Salir\n";
       //
       String entra=JOptionPane.showInputDialog(null, salida);
        //Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
       //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función. 
       //Si oprimimos cancelar, recibiremos un null como resultado.
       int op = Integer.parseInt(entra);
        //Intenger.parseInt: convierte una cadena de texto en un número entero.
       Opciones(op);
       //variable declarada como opciones 
    }
    
    public static void Opciones(int op)throws IOException{
        //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
   //throws: funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
        switch(op){
            //Switch: La instrucción switch es una forma de expresión de un anidamiento múltiple 
        //de instrucciones if ... else.
			case 1: Pila1();
                        //se evalua cada caso para demostrar si la condicion se cumple o no
                                Menu1();
			        break;
      //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.
			case 2: Pila2();
                        //se evalua cada caso para demostrar si la condicion se cumple o no
                                Menu2();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.
			case 3: Comparar();
                        //se evalua cada caso para demostrar si la condicion se cumple o no
                                Menu();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.
			case 4: System.exit(0);
                        //se evalua cada caso para demostrar si la condicion se cumple o no
			        break;
			default:Menu();
     //La cláusula default es opcional y representa las instrucciones que se ejecutarán en caso de que no se 
     //verifique ninguno de los casos evaluados. El último break dentro de un switch 
     //también es opcional, pero lo incluiremos siempre para ser metódicos.
			        break;
      //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.
                        
	   }
    }
    
    public static void Menu1()throws IOException{
    //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
    //throws: funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
       String salida="====Menú Manejo Pila 1====\n"+"1- Insertar elemento en la pila 1\n"+"2- Eliminar elemento en la pila 1\n";
       // //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase, con retorno al elemento declarado
       salida=salida+"3- Buscar elemento en la pila 1\n"+"4- Imprimir pila 1\n"+"5- Contar repetición en la pila 1\n";
       //variable salida que se encargara de buscar el elemento ingresado en la pila 1, de imprimirlo y de contarlo
       salida=salida+"6- Regresar a menú principal\n";
       //variable salida que se encargara de regresar al menu principal
       String entra=JOptionPane.showInputDialog(null, salida);
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
       //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función. 
       //Si oprimimos cancelar, recibiremos un null como resultado.
       int op1 = Integer.parseInt(entra);
       //int: metodo de dato entero
       Opciones1(op1);
       //delcaracion de variable
    }
    
     public static void Opciones1(int op1)throws IOException{
          //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
   //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
   //con un try - catch o no.   
   // TODO code application logic here
        String salio1;
   //dato string variable salio
        switch(op1){
            //Switch: La instrucción switch es una forma de expresión de un anidamiento múltiple 
        //de instrucciones if ... else.
			case 1: Insertar1();
       //se evalua cada caso para demostrar si la condicion se cumple o no                  
                                Menu1();
			        break;
       //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                           
			case 2: salio1=Desapilar1();
      //se evalua cada caso para demostrar si la condicion se cumple o no                     
                                if (!vacia1()){
    //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                                
                                   JOptionPane.showMessageDialog(null, "El dato que salio es "+salio1);
   //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                 
                                }
                                Menu1();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                               
			case 3: Buscar1();
    //se evalua cada caso para demostrar si la condicion se cumple o no                     
                                Menu1();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                            
			case 4: Imprimir1();
    //se evalua cada caso para demostrar si la condicion se cumple o no                     
                                Menu1();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                            
                         case 5: contar1();
    //se evalua cada caso para demostrar si la condicion se cumple o no                      
                                Menu1();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                            
			case 6: 
     //se evalua cada caso para demostrar si la condicion se cumple o no                         
			default:Menu();
    //La cláusula default es opcional y representa las instrucciones que se ejecutarán en caso de que no se 
     //verifique ninguno de los casos evaluados. El último break dentro de un switch 
     //también es opcional, pero lo incluiremos siempre para ser metódicos.
			        break;
          //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                             
                        
	   }
    }
     
     public static void Menu2()throws IOException{
    //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
   //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
       String salida="====Menú Manejo Pila 2====\n"+"1- Insertar elemento en la pila 2\n"+"2- Eliminar elemento en la pila 2\n";
         // //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase, con retorno al elemento declarado
       salida=salida+"3- Buscar elemento en la pila 2\n"+"4- Imprimir pila 2\n"+"5- Contar repetición en la pila 2\n";
       //variable salida que se encargara de buscar el elemento ingresado en la pila 2, de imprimirlo y de contarlo
       salida=salida+"6- Regresar a menú principal\n";
       //variable salida que se encargara de regresar al menu principal
       String entra=JOptionPane.showInputDialog(null, salida);
       //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
       //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función. 
       //Si oprimimos cancelar, recibiremos un null como resultado.
       
       int op2 = Integer.parseInt(entra);
        //int: metodo de dato entero
       Opciones2(op2);
       //declaracion de variable
    }
    
    public static void Opciones2(int op2)throws IOException{
    //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
   //throws: funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
        String salio2;
     // dato de tipo string    
        switch(op2){
     //Switch: La instrucción switch es una forma de expresión de un anidamiento múltiple 
        //de instrucciones if ... else.       
			case 1: Insertar2();
     //se evalua cada caso para demostrar si la condicion se cumple o no                   
                                Menu2();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                           
			case 2: salio2=Desapilar2();
     //se evalua cada caso para demostrar si la condicion se cumple o no                   
                                if (!vacia2()){
      //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                               
                                   JOptionPane.showMessageDialog(null, "El dato que salio es "+salio2);
     //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                               
                                }
                                Menu2();
			        break;
    //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                            
			case 3: Buscar2();
     //se evalua cada caso para demostrar si la condicion se cumple o no                   
                                Menu2();
			        break;
    //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                            
			case 4: Imprimir2();
     //se evalua cada caso para demostrar si la condicion se cumple o no                   
                                Menu2();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                           
                         case 5: contar2();
    //se evalua cada caso para demostrar si la condicion se cumple o no                     
                                Menu2();
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                           
			case 6: 
     //se evalua cada caso para demostrar si la condicion se cumple o no                       
			default:Menu();
      //La cláusula default es opcional y representa las instrucciones que se ejecutarán en caso de que no se 
     //verifique ninguno de los casos evaluados. El último break dentro de un switch 
     //también es opcional, pero lo incluiremos siempre para ser metódicos.                   
			        break;
     //La sentencia de 'break' es de tipo de control de bucles. Dentro de la iteracion en un bucle, 
     //de cualquiera de los tipos (while, do-while, for), el uso de esta sentencia rompe la 
    //iteracion de dicho bucle, es decir si un caso evalua 5 sentencia al insentar un break su unica funcion
    // es la de tener o romper dicha sentencia.                           
                        
	   }
    }
    
    public static void Pila1 (){
 //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
  //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
        
    }
    
    public static void Pila2(){
  //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
  //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.      
        
    }
    
    public static boolean  Comparar() throws IOException{
 //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
//boolean:puede almacenar unicamente dos valores: verdadero o falso. Constantes: true: representa el valor verdadero. 
//false: representa el valor falso.

        if(vacia1()&&vacia2()){
 //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.
            JOptionPane.showMessageDialog(null,"Las pilas estan vacías" );
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.   
            return true;
 //valordevuelto           
        }
               
        else if(cima2==cima1&&cima1==cima2){
 //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
 //de no cumplirse la condición. 
 //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
 //en caso contrario, si es falso (false), se salta dicha sentencia.     
        String quedata1, salida1="";
 //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase. 
        String quedata2, salida2="";
 //string: Dentro de un objeto de la clases String Java crea un array de caracteres 
//A este array se accede a través de las funciones miembro de la clase.        
        int contador1=0;
 //dato de valor entero       
        int contador2=0;
 // dato de valor entero       
            if (cima1!=-1){
 //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.               
                do {
//El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                    
                    quedata1=Desapilar1();
   //quitar elemento almacenados en la pila                 
                    contador1 = contador1+1;
   //contador acumulador                 
                    Apilaraux(quedata1);
   // lista ordenada o estructura de datos que permite almacenar y recuperar datos                 
                }while(cima1!=-1);
  //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.              
                do {
  //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                
                    quedata1=Desapilaraux();
   //quitar elementos almacenados en la pila                 
                    Apilar1(quedata1);
    //lista ordenada o estructura de datos que permite almacenar y recuperar datos                
                }while(cimaaux!=-1);
  //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.               
            }
            if (cima2!=-1)
 //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                
          { do {
   //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.             
                quedata2=Desapilar2();
//quitar elementos almacenados en la pila 
                contador2 = contador2+1;
 // contador acumulador               
                Apilaraux2(quedata2);
 //lista ordenada o estructura de datos que permite almacenar y recuperar datos               
            }while(cima2!=-1);
 //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.          
            do {
   //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.              
                quedata2=Desapilaraux2();
   //quitar elementos almacenados en la pila
                Apilar2(quedata2);
 //  lista ordenada o estructura de datos que permite almacenar y recuperar datos             
            }while(cimaaux2!=-1);
  //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.            
          }
             do {
    //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                
                                 quedata1=Desapilar1();
                      //quitar elementos almacenados en la pila           
                                 salida1=salida1+quedata1+"\n";
          //declaracion de variables con el nombre salida 
                                 Apilaraux(quedata1);     
         //lista ordenada o estructura de datos que permite almacenar y recuperar datos                     
                             }while(cima1!=-1);
     //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse. 
                              
                do {
 //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                 
                            quedata2=Desapilar2();
       //quitar elementos almacenados en la pila                     
                            salida2=salida2+quedata2+"\n";
       //declaracion de variable con el nombre salida                     
                            Apilaraux2(quedata2);    
    //lista ordenada o estructura de datos que permite almacenar y recuperar datos                         
                        }while(cima2!=-1);
      //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.    
                       
                JOptionPane.showMessageDialog(null,"las Pilas son iguales ");
   //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.             
                JOptionPane.showMessageDialog(null,"Elementos en la pila 1: "+ contador1);
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.              
                JOptionPane.showMessageDialog(null,"Elementos en la pila 2: "+ contador2);
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.               
                JOptionPane.showMessageDialog(null,"las Pilas son iguales "+ "\n"+"Pila 1" + "\n"+salida1 +"Pila 2"+"\n"+ salida2);
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.    
                return true;
       //valordevuelto         
        }
        
            JOptionPane.showMessageDialog(null,"las Pilas no son iguales ");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.            
       return false;
  //valor devuelto     
    }
    
    
     public static void Insertar1()throws IOException{
    //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
   //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
       String entra1 = JOptionPane.showInputDialog("Digite un dato para la pila 1");
 //string: Dentro de un objeto de la clases String Java crea un array de caracteres
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.  
       Apilar1(entra1);
//lista ordenada o estructura de datos que permite almacenar y recuperar datos    
    }

    public static void Apilar1(String dato1)throws IOException{
 //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
      if ((Pila1.length-1)==cima1){
  //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.          
        JOptionPane.showMessageDialog(null,"Capacidad de la pila 1 al límite");
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.  
        
        Imprimir1();
  //variable      
      }else{
  //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.              
            cima1++;
            JOptionPane.showMessageDialog(null,"Cima en la posición "+cima1);
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.  
           
            Pila1[cima1]=dato1;
  //se almacenara y se guardara la informacion el la pila 1
      }
    }

    public static void Apilaraux(String dato1)throws IOException{
  //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función.
   //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.
      if ((Pilaaux.length-1)==cimaaux){
 //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                 
        JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar al limite");
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.  
            }else{
  //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
  //de no cumplirse la condición.                    
         cimaaux++;
  //contador        
         Pilaaux[cimaaux]=dato1;
   //acumulador      
       }
    }

    public static boolean vaciaaux(){
 //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
//boolean:puede almacenar unicamente dos valores: verdadero o falso. Constantes: true: representa el valor verdadero. 
//false: representa el valor falso.       
        return (cimaaux==-1);
  //valor de retorno      
    }

    public static boolean vacia1(){
 //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
//boolean:puede almacenar unicamente dos valores: verdadero o falso. Constantes: true: representa el valor verdadero. 
//false: representa el valor falso.       
        return (cima1==-1);
   //valor de retorno     
    }

    public static void Imprimir1()throws IOException{
   //public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
 //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
      String quedata1,salida1=" ";
 //array con metodo string     
      if (cima1!=-1)
    //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.         
      { do {
  //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo. 
            quedata1=Desapilar1();
  //declaracion de variables          
            salida1=salida1+quedata1+"\n";
   //declaracion de variable salida         
            Apilaraux(quedata1);   
   //lista ordenada o estructura de datos que permite almacenar y recuperar datos        
        }while(cima1!=-1);
  //    
        do {
     //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.         
            quedata1=Desapilaraux();
     //declaracion de variable       
            Apilar1(quedata1);
      //variable      
        }while(cimaaux!=-1);
     //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.
        JOptionPane.showMessageDialog(null, salida1);
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.  
      }
      else {
  //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
  //de no cumplirse la condición.                            
          JOptionPane.showMessageDialog(null, "La pila 1 esta vacía");
   //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.         
      }
    }

    public static String Desapilar1()throws IOException{
  //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //String: array con metodo string
   //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.   
      String quedato1;
   //array con metodo string   
      if(vacia1()){
     //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.            
          JOptionPane.showMessageDialog(null,"No se puede eliminar, pila 1 vacía !!!" );
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.              
          return("esta vacia");
 //valor de retorno         
      }else{
  //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.          
              quedato1=Pila1[cima1];
//declaracion de variable              
	      Pila1[cima1] = null;
      //declaracion de variable
	      --cima1;
         //contador     
              return(quedato1);
          //valor de retorno    
            }
    }

    public static String Desapilaraux()throws IOException{
  //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
  //array de tipo string
  //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.   
      String quedato1;
 //array de tipo string     
   //array de tipo string   
      if(cimaaux== -1){
     //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.        
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila 1 vacía !!!" );
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                         
            return("esta vacia");
     //valor de retorno       
      }else{
    //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.            
              quedato1=Pilaaux[cimaaux];
      //delcaracion de variable        
	      Pilaaux[cimaaux] = null;
        //declaracion de variable      
	      --cimaaux;
       //acumulador       
              return(quedato1);
       //valor de retorno       
           }
    }

    public static void Buscar1()throws IOException{
     //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
  //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.    
        if (vacia1()){
  //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                 
            JOptionPane.showMessageDialog(null, "La pila 1 esta vacía");
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                   
        }
        else{
 //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.                  
            String cad1 = JOptionPane.showInputDialog("Digite la cadena a buscar: ");
   //array de tipo string   
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                     
            String quedata1;
   //array de tipo string         
            int bandera1=0; 
 //Una Bandera es una variable booleana que nos indica si ha ocurrido un suceso. Por ejemplo, podemos utilizar 
//una bandera para saber si, tras recorrer una lista de datos, hemos encontrado algún número mayor o menor            
            do {
 //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                        
                    quedata1=Desapilar1();
   // declaracion de variable :desapilar quitar un elemento de la pila                  
                    if(cad1.equals(quedata1)){
      //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                         
                        bandera1=1; 
    //Una Bandera es una variable booleana que nos indica si ha ocurrido un suceso. Por ejemplo, podemos utilizar 
//una bandera para saber si, tras recorrer una lista de datos, hemos encontrado algún número mayor o menor                                
                    }
                    Apilaraux(quedata1);            
//ingresar elemento a una pila, el ultimo elemento en ingresar es el primero en salir                     
            }while(cima1!=-1);
 //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.           
            do {
     //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
                    quedata1=Desapilaraux();
           //desapilar quitar un elemento de la pila auxiliar         
                    Apilar1(quedata1);
        // ingresar un elemento a la pila el ultimo elemento en ingresar es el primero en salir            
            }while(cimaaux!=-1);
  //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.          
            if (bandera1==1) {
   //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                    
                    JOptionPane.showMessageDialog(null,"Elemento encontrado en la pila 1");
       //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                  
            }else{
       //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.                   
                    JOptionPane.showMessageDialog(null,"Elemento no encontrado en la pila 1:()");
  //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                       
                    
            }
        }
    }
    public static void contar1() throws IOException {
       //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
   //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.   
        String quedata1;
   // array de tipo string     
        int contador1=0;
    //contador de tipo entero    
        if (cima1!=-1)
     //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                    
        
          { do {
     //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
         
                quedata1=Desapilar1();
    // desapilar quitar un elemento de la pila            
                contador1 = contador1+1;
          // contador      
                Apilaraux(quedata1);
           // lista ordenada o estructura de datos que permite almacenar y recuperar datos     
            }while(cima1!=-1);
//En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                    
            do {
      //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
               
                quedata1=Desapilaraux();
 //desapilar quitar un elemento de la pila                
                Apilar1(quedata1);
       // lista ordenada o estructura de datos que permite almacenar y recuperar datos          
            }while(cimaaux!=-1);
//En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                      
            JOptionPane.showMessageDialog(null,"Elementos en la pila 1: "+ contador1);
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                  
          }
        else {
 //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.                  
            JOptionPane.showMessageDialog(null, "La pila 1 esta vacía");
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                  
        }
    }           

     public static void Insertar2()throws IOException{
 //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función.          
       String entra2 = JOptionPane.showInputDialog("Digite un dato para la pila 2");
 //los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                        
       Apilar2(entra2);
 //lista ordenada o estructura de datos que permite almacenar y recuperar datos      
    }

    public static void Apilar2(String dato2)throws IOException{
  //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //void: Indica el tipo de objeto que regresa la función. 
  //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.   
 
      if ((Pila2.length-1)==cima2){
  //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                
        JOptionPane.showMessageDialog(null,"Capacidad de la pila 2 al límite");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                            
        Imprimir2();
      }else{
 //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.               
            cima2++;
            JOptionPane.showMessageDialog(null,"Cima en la posición "+cima2);
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
            Pila2[cima2]=dato2;
//lista ordenada o estructura de datos que permite almacenar y recuperar datos            
      }
    }

    public static void Apilaraux2(String dato2)throws IOException{
//public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
//static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto. 
 //Si una lo modifica, el cambio lo ven todas las instancias.
 //void: Indica el tipo de objeto que regresa la función. 
 //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.    
      if ((Pilaaux2.length-1)==cimaaux2){
//if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.          
        JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar 2 al límite");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
//Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
        
      }else{
 //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.                
         cimaaux2++;
  //variable       
         Pilaaux2[cimaaux2]=dato2;
 //lista ordenada o estructura de datos que permite almacenar y recuperar datos        
       }
    }

    public static boolean vaciaaux2(){
//public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
//boolean:puede almacenar unicamente dos valores: verdadero o falso. Constantes: true: representa el valor verdadero. 
//false: representa el valor falso.       
        
        return (cimaaux2==-1);
  //valor de retorno      
    }

    public static boolean vacia2(){
//public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
//boolean:puede almacenar unicamente dos valores: verdadero o falso. Constantes: true: representa el valor verdadero. 
//false: representa el valor falso.       
        
        return (cima2==-1);
//valor de retorno        
    }

    public static void Imprimir2()throws IOException{
//public: El método es público. Cualquier clase puede llamarlo.
 //static: El método es estático. Que un miembro de una clase sea estático quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto.
//void: Indica el tipo de objeto que regresa la función. 
   //En este caso la función no regresa ningún valor, por eso es void.
//twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.   
        
      String quedata2,salida2=" ";
     //array de tipo string 
      if (cima2!=-1)
 //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.               
      { do {
    //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
      
            quedata2=Desapilar2();
      //declaracion de variables      
            salida2=salida2+quedata2+"\n";
       //declaracion de variables     
            Apilaraux2(quedata2);
    //lista ordenada o estructura de datos que permite almacenar y recuperar datos        
        }while(cima2!=-1);
   //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                         
        do {
 //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
           
            quedata2=Desapilaraux2();
    // declaracion e variable desapilar eliminar o quitar un elemento de la pila         
            Apilar2(quedata2);
        //lista ordenada o estructura de datos que permite almacenar y recuperar datos    
        }while(cimaaux2!=-1);
  //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                            
        JOptionPane.showMessageDialog(null, salida2);
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
        
      }
      else {
 //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.                
          JOptionPane.showMessageDialog(null, "La pila 2 esta vacía");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
          
      }
    }

    public static String Desapilar2()throws IOException{
 //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
 //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.     
      String quedato2;
 //declaracion de variables     
      if(vacia2()){
  //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.               
          JOptionPane.showMessageDialog(null,"No se puede eliminar, pila 2 vacía !!!" );
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
          
          return("");
    //valor de retorno      
      }else{
 //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.                
              quedato2=Pila2[cima2];
   //lista ordenada o estructura de datos que permite almacenar y recuperar datos           
	      Pila2[cima2] = null;
    //lista ordenada o estructura de datos que permite almacenar y recuperar datos          
	      --cima2;
     //acumulador         
              return(quedato2);
  //valor de retorno            
            }
    }

    public static String Desapilaraux2()throws IOException{
 //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
  //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.   
      String quedato2;
 //declaracion de variables     
      if(cimaaux2== -1){
  //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.               
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila 2 vacía !!!" );
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
            
            return("");
    //valor de retorno        
      }else{
    //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.             
              quedato2=Pilaaux2[cimaaux2];
        //declaracion de variables      
	      Pilaaux2[cimaaux2] = null;
       //lista ordenada o estructura de datos que permite almacenar y recuperar datos       
	      --cimaaux2;
          //acumulador    
              return(quedato2);
     //valor de retorno         
           }
    }

    public static void Buscar2()throws IOException{
 //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
   //array de tipo string
 //twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.    
        if (vacia2()){
 //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.      
            JOptionPane.showMessageDialog(null, "La pila 2 esta vacía");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
            
        }
        else{
     //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.              
            String cad2 = JOptionPane.showInputDialog("Digite la cadena a buscar: ");
      //array de tipo string  
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
      
            String quedata2;
       //array de tipo string     
            int bandera2=0; 
  //Una Bandera es una variable booleana que nos indica si ha ocurrido un suceso. Por ejemplo, podemos utilizar una bandera para saber si 
//tras recorrer una lista de datos, hemos encontrado algún número mayor o menor          
            do {
    //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
            
                    quedata2=Desapilar2();
    //declaracion de variables
    //quitar un elemento de la pila principal
                    if(cad2.equals(quedata2)){
   //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                           
                        bandera2=1; 
//Una Bandera es una variable booleana que nos indica si ha ocurrido un suceso. Por ejemplo, podemos utilizar 
//una bandera para saber si, tras recorrer una lista de datos, hemos encontrado algún número mayor                        
                    }
                    Apilaraux2(quedata2);  
//lista ordenada o estructura de datos que permite almacenar y recuperar datos                    
            }while(cima2!=-1);
   //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                               
            do {
//El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   

                
                    quedata2=Desapilaraux2();
     //declaracion de variable
     //quitar o eliminar un elemento de la pila principal
                    Apilar2(quedata2);
          //lista ordenada o estructura de datos que permite almacenar y recuperar datos          
            }while(cimaaux2!=-1);
 //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                                 
            if (bandera2==1) {
  //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                    
                    JOptionPane.showMessageDialog(null,"Elemento encontrado en la pila 2");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
                    
            }else{
         //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.              
                    JOptionPane.showMessageDialog(null,"Elemento no encontrado en la pila 2:(");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
                    
            }
        }
    }
    public static void contar2() throws IOException {
 //  //public: El método es público. Cualquier clase con acceso a MainClass puede llamarlo.
   //static: El método es estático. Que un miembro de una clase sea estático quiere decir
   //que todas las instancias de esa clase comparten ese miembro en concreto. 
   //Si una lo modifica, el cambio lo ven todas las instancias.
//array de tipo string
//twrows:funciona para indicarnos que un método lanza una 
    //excepción de un tipo especifico o general, se puede utilizar si la excepción se va manejar 
    //con un try - catch o no.   
        String quedata2;
  //array de tipo string      
        int contador2=0;
  // array de tipo entero      
        if (cima2!=-1)
  //if: Si el resultado del test es verdadero (true) se ejecuta la sentencia que sigue a continuación de if, 
    //en caso contrario, si es falso (false), se salta dicha sentencia.                 
          { do {
//El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
              
                quedata2=Desapilar2();
      //declaracion de variables
      //quitar o eliminar un elemento de la pila
                contador2 = contador2+1;
     //contadores           
                Apilaraux2(quedata2);
      //lista ordenada o estructura de datos que permite almacenar y recuperar datos          
            }while(cima2!=-1);
 //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                               
            do {
 //El bucle do while es muy similar al bucle while. La diferencia radica en cuándo se evalúa 
  //la condición de salida del ciclo.                                   
               
                quedata2=Desapilaraux2();
          //declaracion de variables
          //quitar o eliminar un elemento de la pila auxiliar
                Apilar2(quedata2);
     //lista ordenada o estructura de datos que permite almacenar y recuperar datos           
            }while(cimaaux2!=-1);
 //En el bucle while esta evaluación se realiza antes de entrar al ciclo, 
 //lo que significa que el bucle puede no llegar ejecutarse.                                 
            JOptionPane.showMessageDialog(null,"Elementos en la pila 2: "+ contador2);
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
            
          }
        else {
    //else: La cláusula else (no obligatoria) sirve para indicar instrucciones a realizar en caso 
      //de no cumplirse la condición.               
            JOptionPane.showMessageDialog(null, "La pila 2 esta vacìa");
//los JOptionPane son mensajes que salen en una ventana estas pueden simplemente mostrar un mensaje, 
//dato o pedir un dato, esto se puede usar mediante la libreria en java que es : import javax.swing.*;
//showInputDialog:Este método es una función que muestra una ventana con una caja de texto y dos botones: Aceptar y Cancelar. 
  //Si oprimimos aceptar, recibiremos un String con el contenido de la caja de texto como retorno de la función.                                                                                        
            
        }
    }         
}